gitlab-ci-templates
===================

## Stage : Release

### Semantic-release

Job running [semantic release](https://github.com/semantic-release/semantic-release) from docker to run actions on the repository.

#### Requirements

To run semantic-release with the default configuration we need to be able to use the gitlab release api.
We do not commit a CHANGELOG.md, only the changelog in the project release section so we do not need repository write access.

We need to set up a gitlab-ci a **[protected environment variable](https://docs.gitlab.com/ce/ci/variables/README.html#protected-environment-variables)** to auth semantic release.
But depending on your repository configuration you might need to unprotect the variables `GL_TOKEN` to run the dryRun job without _EGITNOPERMISSION_ error.

##### GitLab authentication

The GitLab authentication configuration is **required** and can be set via
[environment variables](#environment-variables).

Create a [personal access token](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html) with the `api` scope and make it available in your CI environment via the `GL_TOKEN` environment variable. If you are using `GL_TOKEN` as the [remote Git repository authentication](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/ci-configuration.md#authentication) it must also have the `write_repository` scope.

###### Environment variables

| Variable                       | Description                                               |
|--------------------------------|-----------------------------------------------------------|
| `GL_TOKEN` or `GITLAB_TOKEN`   | **Required.** The token used to authenticate with GitLab. |
| `GL_URL` or `GITLAB_URL`       | The GitLab endpoint.                                      |
| `GL_PREFIX` or `GITLAB_PREFIX` | The GitLab API prefix.                                    |


>>>
:bookmark_tabs: Official documentation : [ci-configuration / authentication](https://semantic-release.gitbook.io/semantic-release/usage/ci-configuration#authentication).

> `GL_TOKEN` or `GITLAB_TOKEN` - A GitLab [personal access token](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html).

> _Note: The environment variables GH_TOKEN, GITHUB_TOKEN, GL_TOKEN and GITLAB_TOKEN can be used for both the Git authentication and the API authentication required by @semantic-release/github and @semantic-release/gitlab._

-----------

:bookmark_tabs: Gitlab plugin documentation : [configuration / GitLab authentication](https://github.com/semantic-release/gitlab/blob/master/README.md#configuration).
>>>

#### Configuration

A default `.releaserc.yml` is generated if it's not present in the repository running the job.

```yaml
plugins:
  - '@semantic-release/commit-analyzer'
  - '@semantic-release/release-notes-generator'
  - '@semantic-release/gitlab'
  - '@semantic-release/git'
publish:
  - '@semantic-release/gitlab'
verifyConditions:
  - '@semantic-release/gitlab'
success: false
fail: false
npmPublish: false
```

The default branch is not set.

To use project specific configuration, create and commit a `.releaserc.yml` file in the parent repository.

Example with `CHANGELOG.md` generation when job run on **main** branch :

```yaml
plugins:
  - '@semantic-release/commit-analyzer'
  - '@semantic-release/release-notes-generator'
  - '@semantic-release/gitlab'
  - '@semantic-release/changelog'
  - '@semantic-release/git'
publish:
  - '@semantic-release/gitlab'
verifyConditions:
  - '@semantic-release/gitlab'
success: false
fail: false
npmPublish: false
branches:
  - main
```

#### Gitlab-CI include

- Full example with extend capacities :

```yaml
stages:
  - build
  - test
  - release

include:
  - project: 'botux-fr/ci-cd/gitlab-ci-templates'
    # Change ref to release tag from https://gitlab.com/botux-fr/ci-cd/gitlab-ci-templates/-/releases.
    ref: main
    file: '/release/semantic-release.yml'

build:
  stage: build
  script: echo "building..."

test:
  stage: test
  script: echo "testing..."

release dryRun:
  extends: .semantic-release
  variables:
    EXTRA_ARGS: "-d"
  rules:
    - if: '$CI_COMMIT_BRANCH == "CI_DEFAULT_BRANCH" || $CI_COMMIT_BRANCH == "main"'
      when: never
    - if: '$CI_COMMIT_BRANCH'

release publish:
  extends: .semantic-release
  # The publish job depends on the test job before releasing the new version.
  dependencies:
    - test
  rules:
    - if: '$CI_COMMIT_BRANCH == "CI_DEFAULT_BRANCH" || $CI_COMMIT_BRANCH == "main"'
```

- Simple include with release on `CI_DEFAULT_BRANCH` :

```yaml
stages:
  - release

include:
  - project: 'botux-fr/ci-cd/gitlab-ci-templates'
    # Change ref to release tag from https://gitlab.com/botux-fr/ci-cd/gitlab-ci-templates/-/releases.
    ref: main
    file: '/release/auto-release.yml'
```