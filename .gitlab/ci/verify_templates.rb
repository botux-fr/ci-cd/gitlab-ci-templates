#!/usr/bin/env ruby
# Verify gitlab-ci.yml file via lint gitlab api.
#
# Based from : https://gitlab.com/gitlab-org/security-products/ci-templates/blob/master/scripts/verify_templates.rb

require 'net/http'
require 'json'

LINTER_URI = URI.parse 'https://gitlab.com/api/v4/projects/' + ENV["CI_PROJECT_ID"] + '/ci/lint'

def verify(file)
  content = IO.read(file)
  req = Net::HTTP::Post.new(LINTER_URI)
  req.form_data = {content: content}
  req.add_field("PRIVATE-TOKEN", ENV["GITLAB_TOKEN"])
  response = Net::HTTP.start(LINTER_URI.hostname, LINTER_URI.port, use_ssl: true) { |http| http.request(req) }

  file = file.match(/((\w|\+|#|-|_)+)\.yml/)[1]

  json = JSON.parse(response.body)
  if json['valid'] == true
    puts "\e[32mvalid\e[0m: #{file}" # Color 'valid' green
    puts json['warnings']
  else
    puts "invalid: #{file}"
    puts json['errors']
    exit(1)
  end
end

if ARGV.empty?
  Dir.glob("#{__dir__}/../../**/*.yml").each { |file| verify(file) }

  # Given we test all the templates, the coverage is 100%, always. To showcase
  # how this is done, we print it here.
  # Regexp used to parse this: Coverage:\s(\d+)
  puts 'Coverage: 100%'
else
  verify(ARGV[0])
end
